import java.io.IOException;
import java.util.Scanner;

public class Driver {
	public static void main(String[]args) throws IOException{
		//System.out.println("");
		Scanner s = new Scanner(System.in);
		System.out.println("Creating a course object.");
		// Generate course List
		classGenerator.generate();
		
		// Generate flight list
		flightGenerator.generate();
		
		//Start gui
		GUI gui = new GUI();
		gui.setVisible(true);
	}
}
