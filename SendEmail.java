
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendEmail {

	public SendEmail() {

	}

	public SendEmail(String input) {
		String host = "smtp.gmail.com";
		String port = "587";
		// Recipient's email ID needs to be mentioned.
		String to = "";

		// Sender's email ID needs to be mention
		String from = "";

		// Get system properties
		Properties properties = System.getProperties();
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", port);
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		
		
		//make pw and user name authentication
		SmtpAuthenticator authentication = new SmtpAuthenticator();

		// Get the default Session object.
		Session session = Session.getDefaultInstance(properties,authentication);

		try {
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					to));

			// Set Subject: header field
			message.setSubject("Classes and Flights");

			// Now set the actual message
			message.setText(input);

			// Send message
			Transport.send(message);
			System.out.println("Sent message successfully....");
		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
	}

	public class SmtpAuthenticator extends Authenticator {
		public SmtpAuthenticator() {

			super();
		}

		@Override
		public PasswordAuthentication getPasswordAuthentication() {
			String username = "comp350bot";
			String password = "imonaboat";
			if ((username != null) && (username.length() > 0)
					&& (password != null) && (password.length() > 0)) {

				return new PasswordAuthentication(username, password);
			}

			return null;
		}
	}
}
