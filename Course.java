
public class Course {
	
	private String[] schools = {"CSUCI","CSULB","CSULA","CSUSF"};
	
	private String fullCourseID;
	private String courseNameID;
	private String courseName; 
	private int courseLocation;
	
	private int classNum;
	private int startTime;
	private int endTime;
	
	Course(String id, String courseName, int startTime, int endTime){
			this.classNum = Integer.parseInt(id.substring(2,4));
			this.fullCourseID = id;
			this.courseNameID = id.substring(0,1);
			this.courseName = courseName;
			this.courseLocation = Integer.parseInt(Character.toString(id.charAt(2)));		
			this.startTime = startTime;
			this.endTime = endTime;
	}

	public String getFullCourseID(){
		return this.fullCourseID;
	}

	public String getCourseNameID(){
		return this.courseNameID;
	}

	public String getCourseName(){
		return this.courseName;
	}

	public int getCourseLocation(){
		return this.courseLocation;
	}
	
	public int getClassNum(){
		return this.classNum;
	}

	public int getStartTime(){
		return this.startTime;
	}

	public int getEndTime(){
		return this.endTime;
	}

	public String toString(){
		return "Course Location: "+schools[getCourseLocation()]+
				"\nCourse name: "+getCourseName()+
				"\nGetting course name ID: "+getCourseNameID()+
				"\nClass Num: "+getClassNum()+
				"\nEnd time: "+getEndTime()+
				"\nStart time: "+getStartTime()+
				"\nFull ID: " +this.getFullCourseID();
	}

	public static void populateDataStructure(){ // reads from text file, and makes dict
		
	}


}
