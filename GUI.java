import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.*;


public class GUI extends JFrame{
	
	public GUI(){
		setTitle("Course Flight Planner");
	    setSize(300, 230);
	    setLocationRelativeTo(null);
	    setDefaultCloseOperation(EXIT_ON_CLOSE);
	    
	    JPanel panel = new JPanel(null);
	    getContentPane().add(panel);
	    
	    //quit button
	    JButton quitButton = new JButton("Quit");
	    quitButton.setBounds(0, 180, 300, 30);
	       quitButton.addActionListener(new ActionListener() {
	           public void actionPerformed(ActionEvent event) {
	               System.exit(0);
	          }
	       });
     
	    //enter ur location
	    final String[] authors = {
	    "CSUCI", "CSULB", 
	    "CSULA", "CSUSF", 
	    };
	    
	    JLabel locationLabel = new JLabel("Where are you?");
	    locationLabel.setBounds(25, 30, 150, 30);
	    
	    final JComboBox locationCombo = new JComboBox(authors);
	    locationCombo.setBounds(125, 30, 150, 30);
	    //enter short code
	    JLabel shortCodeLabel = new JLabel("Enter the shortcode of the class:");
	    shortCodeLabel.setBounds(25, 60, 250, 30);
	    final JTextField shortCodeTextField = new JTextField();
	    shortCodeTextField.setBounds(25, 90, 250, 30);
	    //enter full name
	    JLabel classNameLabel = new JLabel("Enter the full name of the class:");
	    classNameLabel.setBounds(25, 120, 250, 30);
	    final JTextField classNameTextField = new JTextField();
	    classNameTextField.setBounds(25, 150, 250, 30);
	    
	    //search button
	    JButton searchButton = new JButton("Search");
	    searchButton.setBounds(0, 0, 300, 30);
	    searchButton.addActionListener(new ActionListener() {
	           public void actionPerformed(ActionEvent event) {
	        	   int campus = locationCombo.getSelectedIndex();
	        	   String course;
	        	   
	        	   if(!(shortCodeTextField.getText().equals(""))){
	        		  course = shortCodeTextField.getText();
	        	   }else{
	        		  course = classNameTextField.getText();
	        	   }
	        	   Database d;
	        	   
				try {
					d = new Database();
					flightGenerator f = new flightGenerator();
					f.generate();
					Course[] stuff = d.FindCourse(course, authors[campus]);	
					StringBuilder dataToEmail = new StringBuilder();
					for(int i = 0; i< stuff.length; i++){
						dataToEmail.append("\n"+stuff[i].toString()+"\n");
						ArrayList<flightGenerator> things = f.getFlights(campus, stuff[i].getCourseLocation(), stuff[i].getStartTime());
						for(flightGenerator flightInfo : things){
							dataToEmail.append(flightInfo.toString()+"\n");
						}
					}
					  System.out.println(dataToEmail.toString());
					  SendEmail s = new SendEmail(dataToEmail.toString());
					 
				} catch (IOException e) {
					e.printStackTrace();
				} 
	          }
	       });
	    //add order
	    panel.add(quitButton);
	    panel.add(searchButton);
	    panel.add(locationLabel);
	    panel.add(locationCombo);
	    panel.add(shortCodeLabel);
	    panel.add(shortCodeTextField);
	    panel.add(classNameLabel);
	    panel.add(classNameTextField);
	  
	    }
}