import java.io.*;
import java.util.*;

public class Database {
	private static Dictionary<String, String> course;
	private static Dictionary<String, Course> ClassInf;
	private static Dictionary<String, CourseIds> courseid;
	
	Database () throws IOException{
		course = new Hashtable<String, String>();
		ClassInf= new Hashtable<String, Course>();
		courseid = new Hashtable<String, CourseIds>();
		classGenerator.generate();
		this.loadCourse("CourseFile.txt");
	}

	public void loadCourse(String theFileName) throws IOException {
		File f = new File(theFileName);
		Scanner s = new Scanner(f);
		while (s.hasNext()) {
			String token = s.nextLine();
			StringTokenizer st = new StringTokenizer(token, "=");
			int i = 0;
			String key = "";
			String value = "";
			while(st.hasMoreElements()){
				i++;
				if(i == 1){
					key = st.nextElement().toString();
				}else if(i == 2){
					value = st.nextElement().toString();
				}
				course.put(key, value);
			}
		}
		s.close();
		this.CourseInformation();
	}
	
	public String searchCourse(String CourseID) throws IOException {
		//System.out.println(CourseID);
		if(CourseID.length() != 2){
			return null;
		}else {
			return course.get(CourseID);
		}

	}
	
	public String CourseInformation() throws IOException{
		String Value;
		String token ="";
		String Id = "";
		String title = "";
		int i = 0;
		int timeout = 0;
		int timein = 0;
		Scanner s = new Scanner(new File("Classes.txt"));
		while (s.hasNext()) {
			token = s.nextLine();
			StringTokenizer st = new StringTokenizer(token, ",");
			while(st.hasMoreTokens()){
			if( i == 0){
				Id = st.nextElement().toString();
				i++;
			} else if (i == 1){
				timein = Integer.parseInt(st.nextElement().toString());
				i++;
			} else if(i == 2){
				timeout = Integer.parseInt(st.nextElement().toString());
				i = 0;
			}
			
				
			Value = token.substring(0, 2);
			title = this.searchCourse(Value);
			}
			this.inputIds(title, Id);
			ClassInf.put(Id, new Course(Id, title, 5, 6)); 
		}
		s.close();
		return token;
	}
	
	public String[] inputIds(String title, String id){
		
		if(courseid.get(title) == null){
			
			courseid.put(title, new CourseIds());
			courseid.get(title).courseArray(id);
			
		}else if(courseid.get(title) != null){
			
			courseid.get(title).courseArray(id);
		}

		return courseid.get(title).getArray();
	}
	
	public Course[] FindCourse(String Course, String campus){
		CourseIds crs;
        Course[] f = null;
		StringBuilder sb = new StringBuilder();
		if(Course.length() == 2){
		crs = this.courseid.get(this.course.get(Course));
		} else {
		crs = this.courseid.get(Course);
		}
		
		if(crs != null){
		String[] a = crs.getArray();
            f = new Course[crs.entries];
            

		for(int k = 0; k < crs.entries; k++){
			Course file = this.ClassInf.get(a[k]);
			
				
			f[k] = file;
			
		}
		}
	
		return f;
	}



}
