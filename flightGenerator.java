import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class flightGenerator {

	private static Map<Integer, flightGenerator> FlightDatabase = new HashMap<Integer, flightGenerator>();
	private int flightNumber;
	private int origin;
	private int destination;
	private int departure;
	private int arrival;

	flightGenerator() {

	}
	
	final String[] authors = {
		    "CSUCI", "CSULB", 
		    "CSULA", "CSUSF", 
		    };
	
	@Override
	public String toString() {
		return "Flight - Flight Number: " + flightNumber + ", Origin: "
				+ authors[origin] + ", Destination: " + authors[destination] + ", Departure Time: "
				+ departure + ", Arrival Time: " + arrival;
	}

	flightGenerator(int number, int origin, int dest, int depart, int arrival) {
		this.flightNumber = number;
		this.origin = origin;
		this.destination = dest;
		this.departure = depart;
		this.arrival = arrival;
	}

	public static void generate() {
		
		Random r = new Random();
		flightGenerator tempFlight;
		for (int i = 0; i < 100; i++) {

			int origin = r.nextInt(4);
			int destination = r.nextInt(4);
			int departure = r.nextInt(24);
			int arrival = departure + r.nextInt(4) + 1;
			if (arrival > 24) {
				arrival = arrival % 24;
			}

			while (origin == destination) {
				destination = r.nextInt(4);
			}
			tempFlight = new flightGenerator(i, origin, destination, departure, arrival);
			FlightDatabase.put(i, tempFlight);

			
		}
		
	}

	public ArrayList<flightGenerator> getFlights(int originInput, int destinationInput, int destinationClassBeginTime) {
		ArrayList<flightGenerator> flightList = new ArrayList<flightGenerator>();
		for (flightGenerator value : FlightDatabase.values()) {
			if (value.origin == originInput && value.destination == destinationInput) {
				flightList.add(value);
			}
		}
		return availableFlights(flightList, destinationClassBeginTime);
	}
	//helper method called by getFlights to find flights
	private ArrayList<flightGenerator> availableFlights(ArrayList<flightGenerator> flightListInput, int destinationClassBeginTime){
		ArrayList<flightGenerator> finalFlightList = new ArrayList<flightGenerator>();
		for( flightGenerator flight : flightListInput){
			if(flight.arrival - destinationClassBeginTime >= .5 ){
				finalFlightList.add(flight);
			}
		}
		return finalFlightList;
	}
}