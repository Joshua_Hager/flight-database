
public class CourseIds {
	public String[] courseId;
	public int entries  = 0;
	
	public CourseIds(){
		courseId = new String[100];
		entries = 0;
	}
	
	public String[] courseArray(String Id){
		courseId[entries] = Id;
		entries++;
		return courseId;
	}
	
	public String[] getArray(){
		return this.courseId;
	}
	
	public int getEntries(){
		return entries;
	}
}
